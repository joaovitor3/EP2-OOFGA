import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Map extends JPanel implements ActionListener {

	private final int SPACESHIP_X = 220;
	private final int SPACESHIP_Y = 430;
	Life life;
	private final Spaceship spaceship;
	private List<Alien> aliens;
	private Player player;
	
	int count = 0;
	
	private boolean inGame;
	private final Image background;
	private final Timer timer_map;

	
	public Map(String name,JFrame mainframe) {
		
		addKeyListener(new KeyListerner());

		setFocusable(true);
		setDoubleBuffered(true);
		ImageIcon image = new ImageIcon("images/space.jpg");

		this.background = image.getImage();
		
		life = new Life(0,0);

		spaceship = new Spaceship(SPACESHIP_X, SPACESHIP_Y);

		inGame = true;

		player = new Player();
		aliens = new ArrayList<Alien>();
		aliens.add(Alien.insertAlien(0));
		
		timer_map = new Timer(Game.getDelay(), this);
		timer_map.start();

	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.drawImage(this.background, 0, 0, null);
		
		if(player.getLifes() == 2){
			life.TwoLifes();			
		}
		else if(player.getLifes() == 1){
			life.oneLife();
		}
		else if(player.getLifes() <= 0){
			life.noLife();
		}	
			
		
		
		if(player.getScore() == 1000){
			inGame = false;
			player.setWinner(true);
		}		
		if (inGame) {
			List<Missile> missile = spaceship.getMissile();

			for (int i = 0; i < missile.size(); i++) {
				Missile m = (Missile) missile.get(i);
				g.drawImage(m.getImage(), m.getX(), m.getY(), this);
			}

			for (int i = 0; i < aliens.size(); i++) {
				Alien in = (Alien) aliens.get(i);
				g.drawImage(in.getImage(), in.getX(), in.getY(), this);
			}			
		}
		else if(player.getWinner() == true){
			drawMissionAccomplished(g);
		}
		
		else {
			drawGameOver(g);
		}
		
		g.drawImage(life.getImage(), Game.getWidth()- life.getWidth(), 0, this);
		drawLife(g);
		drawScore(g);
		draw(g);
		count ++;

		Toolkit.getDefaultToolkit().sync();
	}

	public void collision() {
		Rectangle spriteSpaceship = spaceship.getBounds();
		Rectangle spriteAlien;
		Rectangle spriteMissile;

		for (int i = 0; i < aliens.size(); i++) {
			Alien tempAlien = aliens.get(i);
			spriteAlien = tempAlien.getBounds();

			if (spriteSpaceship.intersects(spriteAlien)) {
				spaceship.setVisible(false);
				tempAlien.setVisible(false);
				player.lossLife();
				if (player.getLifes() <= 0) {					
					inGame = false;
					spaceship.explosion();
				}
			}
		}

		List<Missile> missile = spaceship.getMissile();

		for (int i = 0; i < missile.size(); i++) {
			Missile tempMissile = missile.get(i);
			spriteMissile = tempMissile.getBounds();

			for (int j = 0; j < aliens.size(); j++) {
				Alien tempAlien = aliens.get(j);
				spriteAlien = tempAlien.getBounds();

				if (spriteMissile.intersects(spriteAlien)) {
					tempAlien.setVisible(false);
					tempMissile.setVisible(false);					
					if(!inGame == false){
						player.gainScore(100);
					}
				}
			}
		}
	}

	private void draw(Graphics g) {

		g.drawImage(spaceship.getImage(), spaceship.getX(), spaceship.getY(), this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		List<Missile> missile = spaceship.getMissile();
		count++;
		
		if(count % 10 == 0){
			
			if(player.getScore() <= 5000){
				aliens.add(Alien.insertAlien(0)); 
			}
			else if(player.getScore() >= 10000){
				aliens.add(Alien.insertAlien(2)); 
			}
			else{
				aliens.add(Alien.insertAlien(1)); 
			}
		}		
		if (aliens.size() == 0) {
			inGame = false;
		}		


		if (inGame) {
			//countLevel ++;
			for (int i = 0; i < missile.size(); i++) {
				Missile m = (Missile) missile.get(i);

				if (m.isVisible()) {
					m.move();
				} else {
					missile.remove(i);
				}
			}

			for (int i = 0; i < aliens.size(); i++) {
				Alien in = aliens.get(i);

				if (in.isVisible()) {
					in.move();
				} else {
					aliens.remove(i);
				}
			}
			
		}
		
		updateSpaceship();
		collision();
		repaint();
	}	
	
	private void drawMissionAccomplished(Graphics g) {

		String message = "MISSION ACCOMPLISHED";
		String playAgain = "TO PLAY AGAIN PRESS ENTER";
		Font font = new Font("Helvetica", Font.BOLD, 14);
		FontMetrics metric = getFontMetrics(font);

		g.setColor(Color.white);
		g.setFont(font);
		g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
		g.drawString(message, (Game.getWidth() - metric.stringWidth(playAgain)) / 3, Game.getHeight() / 2);
	}

	private void drawGameOver(Graphics g) {

		String message = "Game Over";
		Font font = new Font("Helvetica", Font.BOLD, 14);
		FontMetrics metric = getFontMetrics(font);

		g.setColor(Color.white);
		g.setFont(font);
		g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
	}
	
	
	private void drawLife(Graphics g) {
		
		String message = "Lifes: ";
		Font font = new Font("Helvetica", Font.BOLD, 14);
		FontMetrics metric = getFontMetrics(font);

		g.setColor(Color.white);
		g.setFont(font);
		
		g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) - life.getWidth() , 14);
	}
	
	private void drawScore(Graphics g) {
		
		
		String message = "Score: " + player.getScore();
		Font font = new Font("Helvetica", Font.BOLD, 14);
		FontMetrics metric = getFontMetrics(font);

		g.setColor(Color.white);
		g.setFont(font);		
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) - 10, 30);

	}

	private void updateSpaceship() {
		spaceship.move();
	}

	private class KeyListerner extends KeyAdapter {

		@Override
		public void keyPressed(KeyEvent e) {
			spaceship.keyPressed(e);
		}

		@Override
		public void keyReleased(KeyEvent e) {
			spaceship.keyReleased(e);
		}

	}
}