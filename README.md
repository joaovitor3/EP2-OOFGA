# Exercício de programação 2 para a disciplina de oo 2017/1 da FGA

# Earth invaders

Repositório destinado ao Exercício de Programação 02 para  a disciplina de OO - Orientação a objetos da Faculdade Unb Gama do primiro semestre de 2017

Aluno: João Vitor Ramos de Souza

Matrícula: 15/0132590

## Sobre

Baseado no icônico jogo ```Space Invaders``` da época do Atari, ```Earth invaders``` não possui nenhum fim lucrativo, existe somente para fins acadêmicos.

O jogo foi desenvolvido através da IDE Eclipse versão 3.8.1-5.1.

## Como rodar o jogo

O jogo foi desenvolvido em Java, utilizando as seguintes ferramentas portanto você deve ter os seguintes programas instalados: 

* [JDK](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html) (Java Development Kit Versão 1.8.0_131): Necessário para quem for contribuir com o projeto.

* [JRE](http://www.oracle.com/technetwork/pt/java/javase/downloads/jre8-downloads-2133155.html) (Java Runtime Envirorment Versão 1.8.0_131-b11)

Para rodar o jogo deve-se realizar os seguintes passos: 
* Importar o projeto do repositório no gitlab:
[gitlab](https://gitlab.com/joaovitor3/EP2-OOFGA/tree/master)

* Abra o Eclipse:

1. Clique em ```File -> New -> Java Project``` e selecione o local em que deseja salvar o projeto;
2. Após criar um java project clique em ```File -> Import... -> ```;
3. A janela de import será aberta. Com ela aberta faça o seguinte passo ```(Aba) General -> (Aba)File System -> (Para ir a próxima janela)Next```;
4. Selecione a pasta do projeto EP2 e clique em ```Ok```;
Obs: O projeto não deverá estar na mesma pasta em que você irá importá-lo;
5. Marque todas as Pastas para a correta importação do projeto;
6. Clique em ```Yes``` em todas mensagens que aparecerem
7. Com o projeto já importado selecione a pasta ```src``` compile o projeto pelo caminho ```Run as -> Java Application```

## Regras

O jogo possui:
* Condição de vitória:
Ao atingir a pontuação de 30.000 pontos o jogador vence o jogo

* Condição de derrota:
Ao colidir com um alien se perde uma vida. Ao perder três vidas o jogador perde o jogo.

* Pontuação:
A cada alien destruído a pontuação é incrementada em 100 pontos.

A ideia do jogo é conseguir destruir o máximo de naves inimigas para poder pontuar e torcer e alcançar a pontuação necessária para vitória, mas cuidado pois você só possui ```3 vidas``` .

## Jogabilidade

Para movimentar sua nave, você pode usar as setas direcionais, e para atirar, utilize a barra de espaço. Ao apertar a barra de espaço um míssel é disparado, se esse atingir um alien o alien é destruido e a pontuação acrescida em 100 pontos. O objetivo é alcançar a pontuação de 30.000

## Créditos

Ideia do jogo: Prof. Renato Coral, junto de seus monitores.

Desenvolvimento: João Vitor Ramos de Souza.

Assets: Algumas foram disponibilizadas pelos monitores, e outra, como a vida foi obtida pelo site opengameart.org

# Bom Jogo!