
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class Spaceship extends Sprite {
    
    private static final int MAX_SPEED_X = 2;
    private static final int MAX_SPEED_Y = 1;
    
    private int speed_x;
    private int speed_y;    

    
    private List<Missile> missile;

    public Spaceship(int x, int y) {
        super(x, y);
        
        missile = new ArrayList<Missile>();
        
        initSpaceShip();
    }

    private void initSpaceShip() {
        
        noThrust();
        
    }
    
    public void explosion(){
    	loadImage("images/explosion.png");
    }
    
    private void noThrust(){
        loadImage("images/spaceship.png"); 
    }
    
    private void thrust(){
        loadImage("images/spaceship_thrust.png"); 
    }    

    public void move() {
        
        if((speed_x < 0 && x <= 0) || (speed_x > 0 && x + width >= Game.getWidth())){
            speed_x = 0;
        }
        
        x += speed_x;
        
        // Limits the movement of the spaceship to the vertical edges.
        if((speed_y < 0 && y <= 0) || (speed_y > 0 && y + height >= Game.getHeight())){
            speed_y = 0;
        }

        y += speed_y;
        
    }
    
    public void shot(){
    	this.missile.add(new Missile(x +getWidth()/3, y + getHeight()/3));
    }
    
    public List<Missile> getMissile(){
    	return missile;
    }
    

    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();
        if (key == KeyEvent.VK_SPACE){
            shot();
        }
        if (key == KeyEvent.VK_LEFT){ 
            speed_x = (-1) * MAX_SPEED_X;
        }

        if (key == KeyEvent.VK_RIGHT){
            speed_x = MAX_SPEED_X;
        }
        
        if (key == KeyEvent.VK_UP){
            speed_y = (-1) * MAX_SPEED_Y;
            thrust();
        }
        
        if (key == KeyEvent.VK_DOWN){
            speed_y = MAX_SPEED_Y;
        }
        
       
    }
    
    public void keyReleased(KeyEvent e) {

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_RIGHT) {
            speed_x = 0;
        }

        if (key == KeyEvent.VK_UP || key == KeyEvent.VK_DOWN) {
            speed_y = 0;
            noThrust();
        }
    }
        
}