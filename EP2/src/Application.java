
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class Application{
    private static JTextField text = new JTextField();    

    public Application() {
        menu();
    }

    public static void game(){
        JFrame game = new JFrame();

        game.add(new Map(text.getText(), game));

        game.setTitle("Space Combat Game");
        game.setSize(Game.getWidth(), Game.getHeight());
        game.setResizable(false);
        game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        game.setLocationRelativeTo(null);
        game.setVisible(true);
    }

    private static void menu(){
        JFrame menu = new JFrame();        
        JPanel panel = new JPanel();
        JButton StartGame = new JButton();        
        JLabel title = new JLabel();     

         
        menu.setTitle("Space Combat Game");
        menu.setSize(Game.getWidth(), Game.getHeight());
        menu.setResizable(false);
        menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        menu.setLocationRelativeTo(null);
        menu.setVisible(true);

        panel.setLayout(null);
        panel.setFocusable(true);
        panel.setDoubleBuffered(true);
        panel.setBackground(Color.black);

        title.setText("Earth Invaders");
        title.setFont(new Font("Courier 10 Pitch", Font.BOLD, 40));
        title.setForeground(Color.white);
        title.setBounds(100, 100, 500, 40);

        StartGame.setText("Start Game");
        StartGame.setBounds((Game.getWidth()-200 ) / 2, 200, 150, 40);
        StartGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                menu.dispose();
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        game();
                    }
                });
            }
        });

        panel.add(title);
        panel.add(StartGame);
        menu.add(panel);

    }
        
    public static void main(String[] args) {

        Application app = new Application();

    }
}