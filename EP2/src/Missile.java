
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

public class Missile extends Sprite{	    
		private static final int WIDTH_SCREEN = 500;
		private static final int SPEED = -3;		
		
	    
	    public Missile(int x, int y) {
	        super(x,y);
	        initMissile();
	    }
	      
	    private void initMissile() {
	        
	        Missile();
	    }
	    
	    private void Missile(){
	        loadImage("images/missile.png");	     
	    }
	    
	    public void move(){
	    	this.y += SPEED;
	    	if(this.x > WIDTH_SCREEN){
	    		setVisible(false);
	    	}
	    } 
}