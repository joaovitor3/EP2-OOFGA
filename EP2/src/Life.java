

public class Life extends Sprite {

    public Life(int x, int y) {
        super(x,y);
        initLife();
        setVisible(false);
    }
      
    private void initLife() {
        
        ThreeLifes();
    }
    
    public void ThreeLifes(){
        loadImage("images/threeLifes.png");        
    }
    
    public void TwoLifes(){
        loadImage("images/twoLifes.png");        
    }
    
    public void oneLife(){
        loadImage("images/oneLife.png");        
    }
    
    public void noLife(){
        loadImage("images/noLife.png");        
    }
}
