
import java.awt.event.KeyEvent;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


public class Alien extends Sprite{
	
	private static final int WIDTH_SCREEN = 500;
	private  int SPEED = 1;
	
    private int speed_x;    
    private int speed_y;
    private int level = 0 ;
    
    
    
	public Alien(int x, int y,int level) {
		super(x, y);
		
        if(level   == 0){
        	initAlien();
        }
        else if (level == 1){
        	initAlienMEDIUM();
        	SPEED = 2;
        }
        else if (level == 2){
        	initAlienHARD();
        	SPEED = 3;
        }
	}

	private void initAlien(){
		alien_EASY();
	}
	
	private void initAlienMEDIUM(){
		alien_MEDIUM();
	}
	
	private void initAlienHARD(){
		alien_HARD();
	}
	
	private void alien_EASY(){
		loadImage("images/alien_EASY.png");
	}
	
	private void alien_MEDIUM(){
		loadImage("images/alien_MEDIUM.png");
	}
	
	private void alien_HARD(){
		loadImage("images/alien_HARD.png");
	}
	
	public int getLevel(){
		return level;
	}
	
	public void setLevel(int level){
		this.level = level;
	}

	
	public void move(){
		if(this.y > 500){
			this.y= WIDTH_SCREEN; 
		}
		else{
			this.y += SPEED;
		}
	}
	
	public static Alien insertAlien(int level){
		Random randx = new Random();
		Random randy = new Random();
		
		return new Alien(randx.nextInt(500) + 10, randy.nextInt(100) - 200, level);
	}	
    
}